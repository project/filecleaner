<?php

/**
 * @file
 * Filecleaner module (filecleaner).
 *
 * This file provide drush commands tasks.
 */

use Drupal\filecleaner\FileCleanerHelper;

/**
 * Implements hook_drush_command().
 */
function filecleaner_drush_command() {
  $items = [];

  // Check an entity type.
  $items['filecleaner'] = [
    'description' => 'Cleanup drupal files.',
    'aliases' => ['fic'],
    'arguments' => [
      'type' => dt('Element to cleanup'),
    ],
    'options' => [
      'exec' => dt('Execute the action, Default is Test mode'),
      'nocr' => dt('Do not rebuild the cache at the end (Not recomended)'),
    ],
    'examples' => [
      'drush fic' => 'Cleanup all (Test mode)',
      'drush fic --exec' => 'Cleanup all (Execute)',
      'drush fic unusedfiles' => 'Cleanup unused files',
      'drush fic emptyfiles' => 'Cleanup empty files (Size = 0)',
      'drush fic styles' => 'Will delete the entier folder located on /sites/[URI]/files/styles',
      'drush fic css' => 'Will delete the entier folder located on /sites/[URI]/files/css',
      'drush fic js' => 'Will delete the entier folder located on /sites/[URI]/files/js',
      'drush fic php' => 'Will delete the entier folder located on /sites/[URI]/files/php',
    ],
  ];
  return $items;
}

/**
 * Call back function of filecleaner.
 */
function drush_filecleaner($type = "all") {

  // Execution mode.
  $exec = drush_get_option('exec');
  if (!$exec) {
    drush_log("Test mode. Add --exec to Execute the action", 'cancel');
  }

  // Remove unused files from managed files system.
  if (FileCleanerHelper::mustRun($type, 'unusedfiles')) {
    $res = FileCleanerHelper::removeUnusedFiles($exec);
    drush_print("Cleanup : Managed files : $res");
  }

  // Remove empty files from managed files system.
  if (FileCleanerHelper::mustRun($type, 'emptyfiles')) {
    $res = FileCleanerHelper::removeEmptyFiles($exec);
    drush_print("Cleanup : Empty files : $res");
  }

  // Remove files.
  $path = \Drupal::service('file_system')->realpath("public://");
  $list = ['styles', 'css', 'js', 'php'];
  foreach ($list as $key) {
    if (FileCleanerHelper::mustRun($type, $key)) {
      $rmdir = $path . "/" . $key;
      if ($exec) {
        $res = exec("rm -rf $rmdir;");
      }
      else {
        $res = exec("du -sh $rmdir;");
        $res = substr($res, 0, strpos($res, "\t"));
      }

      drush_log("Cleanup : $key, $res", $exec ? 'ok' : 'cancel');
    }
  }

  // Cache rebuild.
  if (!drush_get_option('nocr')) {
    $site = drush_get_option('uri') ?: "default";
    exec("drush -l $site cr;");
  }

  drush_print("DONE");
}
