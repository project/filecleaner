File Cleaner
--------------------
This module allow to remove cache files, unused files and/or empty files with
size 0 bytes.

To avoid accidentel deletations of unused files and empty file, you can set the
duration to waite before deletation (Time since last change).

The mudule is usable only via drush (currently).
The option --exec must set to execute action, by default run on test mode.
The option --nocr allow to avoid cache rebuild, but not recomended.

Drush commands
--------------

drush filecleaner
Alias : fic

Fote the examples, type : drush fic --help

CAUTION
- If you use this module, you are conscience what you are doing.
  You are the responsible of your work.



Installation
------------
Download via composer and install via drush (Recommended)
composer require drupal/filecleaner
drush en filecleaner -y

Download and install via drush
drush en filecleaner -y


Configuration
-------------

For the configuration:
- Administration -> Configuration -> Media -> File Cleaner.
- Link : /admin/config/media/filecleaner.

Permission : administer site configuration


------------------ Thank you for using File Cleaner Module. --------------------
