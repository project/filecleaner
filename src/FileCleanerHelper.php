<?php

namespace Drupal\filecleaner;

use Drupal\file\Entity\File;

/**
 * File Cleaner Helper functions.
 */
class FileCleanerHelper {

  /**
   * Get Configuration Name.
   */
  public static function getConfigName() {
    return 'filecleaner.settings';
  }

  /**
   * Get Configuration Object.
   *
   * @param bool $editable
   *   Is editable.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   Configuration object.
   */
  public static function getConfig($editable = FALSE) {
    if ($editable) {
      $config = \Drupal::configFactory()->getEditable(static::getConfigName());
    }
    else {
      $config = \Drupal::config(static::getConfigName());
    }
    return $config;
  }

  /**
   * Get Configuration element value.
   */
  public static function getConfigValue($key) {
    return self::getConfig()->get($key);
  }

  /**
   * Check the operation must run or ignore.
   *
   * @return bool
   *   Run or not
   */
  public static function mustRun($type, $operation) {
    if ($type === 'all') {
      // @todo : if all, compare with default configuration values.
      $res = self::getConfigValue('default_' . $operation);
      return $res ? TRUE : FALSE;
    }
    elseif ($type === $operation) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Remove unused files.
   */
  public static function removeUnusedFiles($exec = FALSE, $timemin = NULL, $filemimie = NULL) {

    $file_usage = \Drupal::service('file.usage');
    $count = 0;
    if (!$timemin) {
      // Minimum duration to allow before deletation.
      $timemin = time() - self::getConfigValue('duration_safe');
    }

    $query = \Drupal::entityQuery('file');
    $query->condition('status', 1);
    $query->condition('changed', $timemin, "<");
    if ($filemimie) {
      $query->condition('filemimie', $filemimie);
    }
    $query->sort('changed', "DESC");
    $ids = $query->execute();
    foreach ($ids as $id) {
      $file = File::load($id);
      $list = $file_usage->listUsage($file);
      if (empty($list)) {
        drush_log("File : " . $file->label(), $exec ? 'ok' : 'cancel');
        $count++;
        if ($exec) {
          $file->delete();
        }
      }
    }
    return $count;
  }

  /**
   * Remove empty files. (Size = 0)
   */
  public static function removeEmptyFiles($exec = FALSE, $timemin = NULL, $filemimie = NULL) {

    $count = 0;
    if (!$timemin) {
      // Minimum duration to allow before deletation.
      $timemin = time() - self::getConfigValue('duration_safe');
    }

    $query = \Drupal::entityQuery('file');
    $query->condition('filesize', 0);
    $query->condition('changed', $timemin, "<");
    if ($filemimie) {
      $query->condition('filemimie', $filemimie);
    }
    $query->sort('changed', "DESC");
    $ids = $query->execute();
    foreach ($ids as $id) {
      $file = File::load($id);
      drush_log("File : " . $file->label(), $exec ? 'ok' : 'cancel');
      $count++;
      if ($exec) {
        $file->delete();
      }
    }
    return $count;
  }

}
