<?php

namespace Drupal\filecleaner\Form;

use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filecleaner\FileCleanerHelper;

/**
 * Filecleaner settings form.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filecleaner_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [FileCleanerHelper::getConfigName()];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = FileCleanerHelper::getConfig();
    $fields = self::getFieldsList();
    $types = [
      'boolean' => 'checkbox',
      'integer' => 'number',
      'string' => 'textfield',
    ];
    foreach ($fields as $field_name => $field_data) {
      $type = $types[$field_data['type']];
      $form[$field_name] = [
        '#type' => $type,
        '#title' => $field_data['label'],
        '#description' => $field_data['description'],
        '#default_value' => $config->get($field_name),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = FileCleanerHelper::getConfig(TRUE);
    $fields = self::getFieldsList();
    foreach ($fields as $field_name => $field_data) {
      $config->set($field_name, $form_state->getValue($field_name));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get fields list from schema file.
   */
  private static function getFieldsList() {
    $path = drupal_get_path('module', 'filecleaner') . "/config/schema/filecleaner.schema.yml";
    $data = Yaml::decode(file_get_contents($path));
    return $data['filecleaner.settings']['mapping'];
  }

}
